﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Tester
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.ReadKey();

            TcpClient client = new TcpClient();
            client.Connect("localhost", 7776);
            Console.WriteLine("Connected!");

            var login = Console.ReadLine();
            var password = Console.ReadLine();

            var sw = new StreamWriter(client.GetStream()) {AutoFlush = true};
            var receiver = new Receiver(new StreamReader(client.GetStream()));

            receiver.StartReceiving();

            sw.WriteLine("{ \"command\": \"login\", \"content\": {\"login\":\""+ login +"\", \"password\":\""+ password +"\"}}");
            sw.WriteLine("{ \"command\": \"matchmaking\" }");

            string json = string.Empty;

            while (!json.Equals("stop", StringComparison.CurrentCultureIgnoreCase))
            {
                json = Console.ReadLine() ?? string.Empty;
                sw.WriteLine(json);    
            }
        }
    }
}
