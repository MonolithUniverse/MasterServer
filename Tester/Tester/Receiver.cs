﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace Tester
{
    internal class Receiver
    {
        private readonly StreamReader sr;

        public Receiver(StreamReader sr)
        {
            this.sr = sr;
        }

        public async void StartReceiving()
        {
            while (true)
            {
                var msg = await ReceiveMsgAsync();
                Console.WriteLine(msg);
            }
        }

        public async Task<string> ReceiveMsgAsync()
        {
            return await this.sr.ReadLineAsync();
        }
    }
}
