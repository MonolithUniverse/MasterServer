﻿using System.IO;
using System.Threading.Tasks;

namespace MasterServer
{
    internal class Receiver
    {
        private readonly StreamReader sr;

        public Receiver(StreamReader sr)
        {
            this.sr = sr;
        }

        public async Task<string> ReceiveMsgAsync()
        {
            return await this.sr.ReadLineAsync();
        }
    }
}
