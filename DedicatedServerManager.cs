﻿using System.ComponentModel;

namespace MasterServer
{
    public class DedicatedServerManager
    {
        private const string UE4EditorPath = @"D:\Program Files\Epic Games\UE_4.15\Engine\Binaries\Win64\UE4Editor.exe";
        private const string ProjectPath = @"E:\Git\NewMonolithUniverse\Game\MonolithUniverse.uproject";

        private int currentPort = Consts.MinDedicatedPort;

        public int StartNewServer()
        {
            var port = GetNextPort();

            var command = $"\"\"{UE4EditorPath}\" \"{ProjectPath}\" MonolithLevel -port={port} -server -log -nosteam\"";
            System.Diagnostics.Process.Start("CMD.exe", $@"/c {command}");

            return port;
        }

        private int GetNextPort()
        {
            if (++currentPort > Consts.MaxDedicatedPort)
                currentPort = Consts.MinDedicatedPort;

            return currentPort;
        }
    }
}