﻿using MasterServer.Commands;
using MasterServer.Helpers;
using Newtonsoft.Json.Linq;

namespace MasterServer.CommandsResolvers
{
    public abstract class CommandBase<T> : ICommand where T: Content
    {
        public virtual bool Authenticate => false;

        public void Execute(Player player, JToken content)
        {
            if (Authenticate && !player.Authenticated)
            {
                player.SendMessage(ErrorHelper.CreateErrorMessage(Consts.Errors.AuthenticationError));
                return;
            }

            Execute(player, content?.ToObject<T>());
        }

        public abstract void Execute(Player player, T content);

    }
}