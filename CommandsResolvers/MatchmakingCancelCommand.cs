﻿using MasterServer.Commands;
using MasterServer.Helpers;

namespace MasterServer.CommandsResolvers
{
    public class MatchmakingCancelCommand : CommandBase<Content>
    {
        public override bool Authenticate => true;

        public override void Execute(Player player, Content content)
        {
            var matchmaker = new Matchmaker();

            if (matchmaker.UnmatchmakePlayer(player))
            {
                player.SendMessage(ResponseHelper.CreateCommandContentMsg(Consts.Commands.MatchmakingCancel));
                LoggerHelper.WriteLine($"Player removed from matchmaking: {player.Username}");
                return;
            }

            player.SendMessage(ErrorHelper.CreateErrorMessage(Consts.Errors.MatchMakingCancelFailed));
        }
    }
}