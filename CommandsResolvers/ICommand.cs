﻿using System;
using MasterServer.Commands;
using Newtonsoft.Json.Linq;

namespace MasterServer.CommandsResolvers
{
    internal interface ICommand
    {
        bool Authenticate { get; }
        void Execute(Player player, JToken content);
    }
}