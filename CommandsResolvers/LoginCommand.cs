﻿using MasterServer.Commands.RqContent;
using MasterServer.DAL.Services;
using MasterServer.Helpers;

namespace MasterServer.CommandsResolvers
{
    internal class LoginCommand : CommandBase<LoginRqContent>
    {
        private readonly UserService userService = new UserService();

        public override void Execute(Player player, LoginRqContent content)
        {
            if (player.Authenticated)
            {
                player.SendMessage(ErrorHelper.CreateErrorMessage(Consts.Errors.AlreadyLoggedIn));
                return;
            }

            var status = userService.ValidateUser(content.Login, content.Password);

            if (status.Failed)
            {
                player.SendMessage(ErrorHelper.CreateErrorMessage(status));
                return;
            }

            player.Authneticate(status.Message);
            player.SendMessage(ResponseHelper.CreateCommandContentMsg(Consts.Commands.Login, new {username = status.Message}));
            LoggerHelper.WriteLine($"User logged in: {status.Message}");
        }
    }
}
