﻿using MasterServer.Commands;
using MasterServer.Helpers;

namespace MasterServer.CommandsResolvers
{
    public class MatchmakingCommand : CommandBase<Content>
    {
        public override bool Authenticate => true;

        public override void Execute(Player player, Content content)
        {
            var matchMaker = new Matchmaker();

            if (matchMaker.MatchmakePlayer(player))
            {
                player.SendMessage(ResponseHelper.CreateCommandContentMsg(Consts.Commands.Matchmaking));
                LoggerHelper.WriteLine($"User added to matchmaking: {player.Username}");
                return;
            }

            player.SendMessage(ErrorHelper.CreateErrorMessage(Consts.Errors.MatchmakingFailed));
        }
    }
}