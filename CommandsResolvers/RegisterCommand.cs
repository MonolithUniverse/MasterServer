﻿using System.CodeDom;
using MasterServer.Commands.RqContent;
using MasterServer.DAL.Services;
using MasterServer.Helpers;

namespace MasterServer.CommandsResolvers
{
    public class RegisterCommand : CommandBase<RegisterRq>
    {
        private readonly UserService userService = new UserService();

        public override void Execute(Player player, RegisterRq content)
        {
            var status = userService.CreateUser(content.Login, content.Password, content.Username);

            if (status.Failed)
            {
                player.SendMessage(ErrorHelper.CreateErrorMessage(status.Message));
                return;
            }

            player.SendMessage(ResponseHelper.CreateCommandContentMsg(Consts.Commands.Register));
            LoggerHelper.WriteLine($"New user registered: {content.Username}");
        }
    }
}