﻿using System.IO;
using System.Threading.Tasks;

namespace MasterServer
{
    public class Sender
    {
        private readonly StreamWriter sw;

        public Sender(Stream stream)
        {
            this.sw = new StreamWriter(stream) {AutoFlush = true};
        }

        public async Task SendMessageAsync(string msg)
        {
             await sw.WriteLineAsync(msg);
        }
    }
}
