﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using MasterServer.CommandsResolvers;
using Newtonsoft.Json.Linq;
using ICommand = MasterServer.CommandsResolvers.ICommand;

namespace MasterServer
{
    internal class PlayerCommandDispatcher
    {
        private readonly Player player;
        private readonly Receiver receiver;

        private readonly Dictionary<string, ICommand> commands = new Dictionary<string, ICommand>();

        private bool isRunning = true;

        public async Task RunDispatcherAsync()
        {
            while (isRunning)
            {
                try
                {
                    await AcceptCommandAsync();
                }
                catch (IOException)
                {
                    StopDispatcher();
                }
            }
        }

        public void StopDispatcher()
        {
            isRunning = false;
        }

        public PlayerCommandDispatcher(Player player, Receiver receiver)
        {
            this.player = player;
            this.receiver = receiver;

            RegisterCommandResolvers();
        }

        private void RegisterCommandResolvers()
        {
            commands.Add(Consts.Commands.Login, new LoginCommand());
            commands.Add(Consts.Commands.Register, new RegisterCommand());
            commands.Add(Consts.Commands.Matchmaking, new MatchmakingCommand());
            commands.Add(Consts.Commands.MatchmakingCancel, new MatchmakingCancelCommand());
        }

        public async Task AcceptCommandAsync()
        {
            var json = await this.receiver.ReceiveMsgAsync();
            var jObject = JObject.Parse(json);

            var jCommand = jObject[Consts.Command];
            var jContent = jObject[Consts.Content];

            var command = this.commands[jCommand.ToObject<string>()];
            command.Execute(this.player, jContent);
        }
    }
}
