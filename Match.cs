﻿namespace MasterServer
{
    public class Match
    {
        public Player[] BlueTeam { get; set; }
        public Player[] RedTeam { get; set; }
    }
}