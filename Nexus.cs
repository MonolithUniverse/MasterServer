﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using MasterServer.Helpers;

namespace MasterServer
{
    public class Nexus
    {
        private readonly TcpListener server;
        private bool isRunning = true;

        public Nexus(int port)
        {
            server = new TcpListener(IPAddress.Any, port);
        }

        public async void StartServer()
        {
            this.server.Start();
            LoggerHelper.WriteLine("Server started");

            while (this.isRunning)
            {
                var client = await server.AcceptTcpClientAsync();
                RunPlayerRoutine(client);
            }
        }

        private async void RunPlayerRoutine(TcpClient client)
        {
            var player = new Player(client);
            var receiver = new Receiver(new StreamReader(client.GetStream()));
            var dispatcher = new PlayerCommandDispatcher(player, receiver);

            LoggerHelper.WriteLine("New user connected");
            await dispatcher.RunDispatcherAsync();

            player.Disconnect();
            LoggerHelper.WriteLine($"User disconnected: {player.Username}");
        }


        public void Stop()
        {
            this.isRunning = false;
        }
    }
}