﻿namespace MasterServer.Commands
{
    public class ServerMessage<T> where T : Content
    {
        public string Command { get; set; }
        public T Content { get; set; }
    }
}
