﻿namespace MasterServer.Commands.RqContent
{
    public class LoginRqContent : Content
    {
        public string Login { get; set; }
        public string Password { get; set; }
    }
}
