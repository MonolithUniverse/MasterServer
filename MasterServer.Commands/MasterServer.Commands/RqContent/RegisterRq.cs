﻿namespace MasterServer.Commands.RqContent
{
    public class RegisterRq : Content
    {
        public string Login { get; set; }
        public string Password { get; set; }
        public string Username { get; set; }
    }
}