﻿using System;
using System.Security.Cryptography;
using MasterServer.DAL.Models;
using MongoDB.Bson;
using MongoDB.Driver;

namespace MasterServer.DAL.Services
{
    public class UserService
    {
        private readonly IMongoCollection<User> users = DbConnector.GetCollection<User>(Consts.Collections.Users);

        public Status CreateUser(string login, string password, string username)
        {
            var user = GetUserFromDatabaseByLogin(login);

            if (user != null)
                return Status.Fail(Consts.Errors.LoginAlreadyExist);

            user = GetUserFromDatabaseByUsername(username);

            if(user != null)
                return Status.Fail(Consts.Errors.UsernameAlreadyExists);

            var hashedPassword = HashPassword(password);

            user = new User
            {
                Login = login,
                Password = hashedPassword,
                Username = username
            };

            users.InsertOne(user);

            return Status.Ok();
        }

        private string HashPassword(string password)
        {
            var salt = new byte[16];
            var cryptic = new RNGCryptoServiceProvider();

            cryptic.GetBytes(salt);

            var pbkdf2 = new Rfc2898DeriveBytes(password, salt, 10000);
            var hash = pbkdf2.GetBytes(20);

            var hashBytes = new byte[36];

            Array.Copy(salt, 0, hashBytes, 0, 16);
            Array.Copy(hash, 0, hashBytes, 16, 20);

            return Convert.ToBase64String(hashBytes);
        }

        public Status ValidateUser(string login, string password)
        {
            var user = GetUserFromDatabaseByLogin(login);

            if (user == null)
                return Status.Fail(Consts.Errors.LoginDoesntExist);

            var hashBytes = Convert.FromBase64String(user.Password);
            var salt = new byte[16];

            Array.Copy(hashBytes, 0, salt, 0, 16);

            var pbkdf2 = new Rfc2898DeriveBytes(password, salt, 10000);
            var hash = pbkdf2.GetBytes(20);

            if (!CompareHashes(hashBytes, hash))
                return Status.Fail(Consts.Errors.PasswordDoesntMatch);

            return Status.Ok(user.Username);
        }

        private User GetUserFromDatabaseByLogin(string login)
        {
            var filter = Builders<User>.Filter.Eq(x => x.Login, login);
            return users.Find(filter).FirstOrDefault();
        }

        private User GetUserFromDatabaseByUsername(string username)
        {
            var filter = Builders<User>.Filter.Eq(x => x.Username, username);
            return users.Find(filter).FirstOrDefault();
        }

        private bool CompareHashes(byte[] hashBytes, byte[] hash)
        {
            for (var i = 0; i < 20; i++)
            {
                if (hashBytes[i + 16] != hash[i])
                    return false;
            }

            return true;
        }
    }
}