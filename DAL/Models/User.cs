﻿using MongoDB.Bson.Serialization.Attributes;

namespace MasterServer.DAL.Models
{
    [BsonIgnoreExtraElements]
    public class User
    {
        public string Username { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
    }
}