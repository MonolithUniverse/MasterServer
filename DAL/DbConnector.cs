﻿using System;
using System.CodeDom;
using System.Security.Cryptography;
using MongoDB.Bson;
using MongoDB.Driver;

namespace MasterServer.DAL
{
    public static class DbConnector
    {
        private static readonly Lazy<MongoClient> client = new Lazy<MongoClient>(() => new MongoClient());
        private static readonly Lazy<IMongoDatabase> database = new Lazy<IMongoDatabase>(() => client.Value.GetDatabase(Consts.Mongo.DatabaseName));

        public static IMongoCollection<T> GetCollection<T>(string collectionName)
        {
            return database.Value.GetCollection<T>(collectionName);
        }
    }
}