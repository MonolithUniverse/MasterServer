﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading.Tasks;
using MasterServer.Helpers;

namespace MasterServer
{
    public class Matchmaker
    {
        private static readonly DedicatedServerManager dedicatedServerManager = new DedicatedServerManager();

        private static readonly ConcurrentDictionary<string, Player> matchmakingPlayers = new ConcurrentDictionary<string, Player>();
        private bool IsPossibleToCreateMatch => matchmakingPlayers.Count >= 4;

        public bool MatchmakePlayer(Player player)
        {
            var status = matchmakingPlayers.TryAdd(player.Username, player);

            if (status)
                Task.Run(() => CheckMatchmakingStatus());

            return status;
        }

        public bool UnmatchmakePlayer(Player player)
        {
            return matchmakingPlayers.TryRemove(player.Username, out player);
        }

        private void CheckMatchmakingStatus()
        {
            if (IsPossibleToCreateMatch)
                CreateMatch();
        }

        private async void CreateMatch()
        {
            var port = dedicatedServerManager.StartNewServer();

            var blueTeam = GetPlayersForTeam();
            var redTeam = GetPlayersForTeam();

            SendMatchFoundMessage(blueTeam, Consts.Team.Blue);
            SendMatchFoundMessage(redTeam, Consts.Team.Red);

            await Task.Delay(TimeSpan.FromSeconds(10));

            SendMatchStartMessage(blueTeam, port, Consts.Team.Blue);
            SendMatchStartMessage(redTeam, port, Consts.Team.Red);
        }

        private void SendMatchFoundMessage(Player[] players, string teamName)
        {
            var command = ResponseHelper.CreateCommandContentMsg(Consts.Commands.MatchFound, new {allies = players.Select(x => x.Username), team = teamName});

            foreach (var player in players)
            {
                player.SendMessage(command);
            }
        }

        private void SendMatchStartMessage(Player[] players, int port, string teamName)
        {
            var command = ResponseHelper.CreateCommandContentMsg(Consts.Commands.MatchStart, new {port = port, team = teamName});

            foreach (var player in players)
            {
                player.SendMessage(command);
            }
        }

        private Player[] GetPlayersForTeam()
        {
            var players = matchmakingPlayers.Values.Take(2).ToArray();
            RemovePlayersFromMatchMaking(players);
            return players;
        }

        private void RemovePlayersFromMatchMaking(Player[] players)
        {
            foreach (var player in players)
            {
                Player removed;
                matchmakingPlayers.TryRemove(player.Username, out removed);
            }
        }
    }
}