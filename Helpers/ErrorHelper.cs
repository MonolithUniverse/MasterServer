﻿using Newtonsoft.Json;

namespace MasterServer.Helpers
{
    public class ErrorHelper
    {
        public static string CreateErrorMessage(string message)
        {
            var error = new {command = Consts.Commands.Error, content = new {message = message}};
            return JsonConvert.SerializeObject(error);
        }

        public static string CreateErrorMessage(Status status)
        {
            return CreateErrorMessage(status.Message);
        }
    }
}