﻿using Newtonsoft.Json;

namespace MasterServer.Helpers
{
    public static class ResponseHelper
    {
        public static string CreateCommandContentMsg(string command)
        {
            var message = new { command = command };
            return JsonConvert.SerializeObject(message);
        }

        public static string CreateCommandContentMsg<T>(string command, T content) where T : class
        {
            var message = new {command = command, content = content};
            return JsonConvert.SerializeObject(message);
        }
    }
}