﻿using System;

namespace MasterServer.Helpers
{
    public static class LoggerHelper
    {
        public static void WriteLine(string message)
        {
            Console.WriteLine(message);
        }
    }
}