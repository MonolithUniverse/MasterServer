﻿namespace MasterServer
{
    public class Consts
    {
        public const int MatchmakingPort = 7776;
        public const int MinDedicatedPort = 7777;
        public const int MaxDedicatedPort = 60000;

        public const string GuestUsername = "Guest";

        public class Mongo
        {
            public const string DatabaseName = "MonolithUniverse";
        }

        public class Collections
        {
            public const string Users = "Users";
        }

        public const string Command = "command";
        public const string Content = "content";
        public class Commands
        {
            public const string Login = "login";
            public const string Register = "register";
            public const string Lobby = "lobby";
            public const string Error = "error";
            public const string Matchmaking = "matchmaking";
            public const string MatchmakingCancel = "matchmakingCancel";
            public const string MatchFound = "matchFound";
            public const string MatchStart = "matchStart";
        }

        public class Errors
        {
            public const string AuthenticationError = "You have to be logged in to perform this action";
            public const string LoginDoesntExist = "Selected login doesn't exist";
            public const string PasswordDoesntMatch = "Selected password is invalid";
            public const string LoginAlreadyExist = "Selected login already exists";
            public const string UsernameAlreadyExists = "Selected username already exists";
            public const string AlreadyLoggedIn = "You are already logged in";
            public const string MatchmakingFailed = "MatchmakingCommand failed for unknown reason. Try again.";
            public const string MatchMakingCancelFailed = "Couldn't stop looking for a match for unknown reason. Try again.";
        }

        public class Team
        {
            public const string Blue = "blue";
            public const string Red = "red";
        }
    }
}