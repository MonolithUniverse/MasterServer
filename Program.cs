﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace MasterServer
{
    class Program
    {
        private static readonly Nexus nexus = new Nexus(7776);

        static void Main(string[] args)
        {
            Task.Run(() => nexus.StartServer());

            string line;
            do
            {
                line = Console.ReadLine();
            } while (line == null || !line.Equals("exit"));

            nexus.Stop();
        }
    }
}
