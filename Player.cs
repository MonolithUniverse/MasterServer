﻿using System.Net.Sockets;
using MasterServer.Helpers;

namespace MasterServer
{
    public class Player
    {
        private readonly TcpClient client;
        private readonly Sender sender;

        public bool Authenticated { get; private set; }
        public string Username { get; private set; }

        public Player(TcpClient client)
        {
            this.Authenticated = false;
            this.Username = Consts.GuestUsername;
            this.client = client;
            this.sender = new Sender(client.GetStream());
        }

        public async void SendMessage(string message)
        {
            await sender.SendMessageAsync(message);
        }

        public void Authneticate(string username)
        {
            this.Authenticated = true;
            this.Username = username;
        }

        public void Disconnect()
        {
            if (new Matchmaker().UnmatchmakePlayer(this))
            {
                LoggerHelper.WriteLine($"Player removed from matchmaking: {this.Username}");
            }
            this.client.Close();
        }
    }
}
