﻿namespace MasterServer
{
    public class Status
    {
        public bool Success { get; set; }
        public string Message { get; set; }

        public bool Failed => !Success;

        public static Status Ok(string message = null)
        {
            return new Status
            {
                Success = true,
                Message = message
            };
        }

        public static Status Fail(string message = null)
        {
            return new Status
            {
                Success = false,
                Message = message
            };
        }
    }
}